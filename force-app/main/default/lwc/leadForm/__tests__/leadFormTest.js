/**
 * Created by Scott McClung on 10/4/20.
 */

import {createElement} from 'lwc';
import leadForm from "../leadForm";

afterEach(() => {
    // The jsdom instance is shared across test cases in a single file so reset the DOM
    while (document.body.firstChild) {
        document.body.removeChild(document.body.firstChild);
    }
});

describe('the lead form',() => {
   beforeEach(() => {

   })

    it('should not be submittable before the form has changed', () => {
        const element = createElement('c-lead-form', {
            is: leadForm
        });
        document.body.appendChild(element);
        const submitButton = element.shadowRoot.querySelector('lightning-button');
        return Promise.resolve().then(() => {
           expect(submitButton).not.toBeNull();
           expect(submitButton.disabled).toBeTruthy();
       });
   });

   it('should be submittable when the form is dirty', () => {
       const element = createElement('c-lead-form', {
           is: leadForm
       });
       document.body.appendChild(element);
       const form = element.shadowRoot.querySelector('lightning-record-edit-form');
       const submitButton = element.shadowRoot.querySelector('lightning-button');
       const firstName = form.querySelector('lightning-input-field');
       firstName.value = "Something";
       return Promise.resolve().then(() => {
           expect(submitButton).not.toBeNull();
           expect(submitButton.disabled).toBeTruthy();
       });
   })
});