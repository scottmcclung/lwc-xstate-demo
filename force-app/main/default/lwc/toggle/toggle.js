import {LightningElement, track} from 'lwc';
import {createMachine, interpret} from "c/finiteStateMachine";

const toggleMachine = createMachine({
    id:      'toggle',
    initial: 'inactive',
    states:  {
        inactive: {on: {click: 'active'}},
        active:   {on: {click: 'inactive'}}
    }
});

const {send, subscribe} = interpret(toggleMachine).start();


export default class StopLight extends LightningElement {
    @track state = {};

    get active() {
        return this.state.matches('active');
    }

    handleClick(e) {
        send(e);
    }

    connectedCallback() {
        this.subscription = subscribe((state) => this.state = state);
    }

    disconnectedCallback() {
        this.subscription.unsubscribe();
    }
}

